# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect 
from django.template import Context, Template
from django.template.loader import get_template
from MyFallApp.GameManager import *
from MyFallApp.models import *
import json
import datetime
import sys

# Create your views here.
if sys.argv[1] == 'runserver':
    gm = GameManager()
    gm.start()

def to_properties_json(obj):
    properties = {}
    for attr in dir(type(obj)):
        if isinstance(getattr(type(obj), attr), property):
            properties[attr] = getattr(obj, attr)

    return json.dumps(properties)


def get_cookie_content(cookie):
    cookies = cookie.decode('base64').split('&')
    attributes = {}
    for c in cookies:
        key, value = c.split('=')
        attributes[key] = value.decode('utf-8')
    return attributes


def validate_cookie(cookies):
    if 'credentials' not in cookies:
        return False
    cookie = cookies['credentials']
    players = [player.player_name for player in Player.objects.all()]
    try:
        attributes = get_cookie_content(cookie)
        return attributes['p'] in players and attributes['dn']
    except:
        return False

def login(request):
    players = [player.player_name for player in Player.objects.all()]
    context = {'players' : players}
    response = HttpResponse(get_template('login.html').render(context, request))
    return response

def connect(request):
    display_name = request.POST.get('dn', '').replace('&', '')
    player_name = request.POST.get('p', '').replace('&', '')
    players = [player.player_name for player in Player.objects.all()]
    if player_name in players:
        response = HttpResponse('OK')
    else:
        response = HttpResponse('Can not find this user')
    max_age = 60 * 60 * 5
    expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age), "%a, %d-%b-%Y %H:%M:%S GMT")
    cookie = 'p={player}&dn={display_name}'.format(player=player_name.encode('utf-8'), display_name=display_name.encode('utf-8')).encode('base64')
    response.set_cookie('credentials', cookie, expires=expires)
    return response

def games(request):
    if not validate_cookie(request.COOKIES):
        return HttpResponseRedirect('/login/')
    return HttpResponse(get_template('games.html').render({'games' : gm.games.values()}, request))

def games_list(request):
    if not validate_cookie(request.COOKIES):
        return HttpResponseRedirect('/login/')
    player = get_cookie_content(request.COOKIES['credentials'])['p']
    player_game = gm.get_game_player_in(player) 
    if player_game is not None:
        games = [to_properties_json(player_game)]
        player_game.keepalive(player)
    else:
        games = [to_properties_json(game) for game in gm.games.values()]
    response = HttpResponse(json.dumps(games))
    in_game = player_game is not None
    response['inGame'] = in_game
    if in_game: 
        response['Role'] = player_game.get_player_role(player)
    return response

def join_game(request):
    if not validate_cookie(request.COOKIES):
        return HttpResponseRedirect('/login/')
    try:
        attributes = get_cookie_content(request.COOKIES['credentials'])
        player = attributes['p']
        display_name = attributes['dn']
        game_code = request.GET.get('gc', '')
        if game_code and gm.join_game(game_code, player, display_name):
            message = 'GOOD'
        else:
            message = 'BAD'
        return HttpResponse(message)
    except:
        return HttpResponse('BAD')

def quit_game(request):
    if not validate_cookie(request.COOKIES):
        return HttpResponseRedirect('/login/')
    attributes = get_cookie_content(request.COOKIES['credentials'])
    player = attributes['p']
    gm.remove_player(player)
    return HttpResponse('OK')


def create_game(request):
    if not validate_cookie(request.COOKIES):
        return HttpResponseRedirect('/login/')
    attributes = get_cookie_content(request.COOKIES['credentials'])
    player = attributes['p']
    display_name = attributes['dn']
    game_code = request.POST.get('gc', '')
    response_message = ''
    try:
        if gm.open_game(game_code, player, display_name):
            response_message = 'OK'
        else:
            response_message = 'BAD'
    except:
        response_message = 'BAD'
    return HttpResponse(response_message)

def manage(request):
    return HttpResponse(get_template('manage.html').render({'games' : gm.games.values()}, request))

def manage_list(request):
    values = {}
    values['players'] = [p.player_name for p in Player.objects.all()]
    values['groups'] = [g.group_name for g in PlayersGroup.objects.all()]
    values['groups_list'] = {g.group_name : json.dumps(g.get_players()) for g in PlayersGroup.objects.all()}
    values['places'] = [p.place_name for p in Place.objects.all()]
    values['sets'] = [s.set_name for s in PlacesSet.objects.all()]
    values['sets_list'] = {s.set_name : json.dumps(s.get_places()) for s in PlacesSet.objects.all()}
    values_json = json.dumps(values)
    return HttpResponse(values_json)

def remove_obj(request):
    try:
        obj_name = request.POST['obj_name'].translate('&"\'')
        list_name = request.POST['list_name'].translate('&"\'')
        if list_name == 'places':
            Place.get_place(obj_name).remove()
        elif list_name == 'sets':
            PlacesSet.get_set(obj_name).delete()
        elif list_name == 'players':
            Player.get_player(obj_name).remove()
        elif list_name == 'groups':
            PlayersGroup.get_group(obj_name).delete()
        return HttpResponse('OK')
    except:
        return HttpResponse('BAD')

def add_obj(request):
    try:
        obj_name = request.POST['obj_name'].translate('&"\'')
        list_name = request.POST['list_name'].translate('&"\'')
        assert obj_name and list_name
        if list_name == 'places':
            p = Place()
            p.place_name = obj_name
            p.clean()
            p.save()
        elif list_name == 'sets':
            p = PlacesSet()
            p.set_name = obj_name
            p.clean()
            p.save()
        elif list_name == 'players':
            p = Player()
            p.player_name = obj_name
            p.clean()
            p.save()
        elif list_name == 'groups':
            p = PlayersGroup()
            p.group_name = obj_name
            p.clean()
            p.save()
        return HttpResponse('OK')
    except:
        return HttpResponse('BAD')

def link_obj(request):
    try:
        obj_name = request.POST['obj_name'].translate('&"\'')
        link_to = request.POST['link_to'].translate('&"\'')
        list_name = request.POST['list_name'].translate('&"\'')
        assert obj_name and list_name and link_to in ['groups', 'sets']
        if link_to == 'sets':
            set_obj = PlacesSet.get_set(list_name)
            set_obj.add_place_to_set(obj_name)
        elif link_to == 'groups':
            group_obj = PlayersGroup.get_group(list_name)
            group_obj.add_player_to_group(obj_name)
        return HttpResponse('OK')
    except:
        return HttpResponse('BAD')

def unlink_obj(request):
    try:
        obj_name = request.POST['obj_name'].translate('&"\'')
        link_to = request.POST['link_to'].translate('&"\'')
        list_name = request.POST['list_name'].translate('&"\'')
        assert obj_name and list_name and link_to in ['groups', 'sets']
        if link_to == 'sets':
            set_obj = PlacesSet.get_set(list_name)
            set_obj.remove_place_from_set(obj_name)
        elif link_to == 'groups':
            group_obj = PlayersGroup.get_group(list_name)
            group_obj.remove_player_from_group(obj_name)
        return HttpResponse('OK')
    except:
        return HttpResponse('BAD')

def get_players(request):
    try:
        players = [p.player_name for p in Player.objects.all()]
        return HttpResponse(json.dumps(players))
    except:
        return HttpResponse('BAD')

def get_sets(request):
    try:
        sets = [s.set_name for s in PlacesSet.objects.all()]
        return HttpResponse(json.dumps(sets))
    except:
        return HttpResponse('BAD')

def start_game(request):
    if not validate_cookie(request.COOKIES):
        return HttpResponseRedirect('/login/')
    try:
        player = get_cookie_content(request.COOKIES['credentials'])['p']
        player_game = gm.get_game_player_in(player)
        set_name = request.POST['set_name'].translate('&"\'')
        number_of_spies = int(request.POST['num_spies'].translate('&"\''))
        assert number_of_spies >= 0 and number_of_spies <= len(player_game.players)
        if (gm.start_game(player_game.code, player, set_name, number_of_spies)):
            return HttpResponse('OK')
        return HttpResponse('BAD')
    except:
        return HttpResponse('BAD')

def stop_game(request):
    if not validate_cookie(request.COOKIES):
        return HttpResponseRedirect('/login/')
    try:
        player = get_cookie_content(request.COOKIES['credentials'])['p']
        player_game = gm.get_game_player_in(player)
        if (gm.stop_game(player_game.code, player)):
            return HttpResponse('OK')
        return HttpResponse('BAD')
    except:
        return HttpResponse('BAD')

def get_current_places(request):
    if not validate_cookie(request.COOKIES):
        return HttpResponseRedirect('/login/')
    try:
        player = get_cookie_content(request.COOKIES['credentials'])['p']
        player_game = gm.get_game_player_in(player)
        if (player_game.is_started):
            return HttpResponse(json.dumps(player_game.places))
        return HttpResponse('BAD')
    except:
        return HttpResponse('BAD')

def get_current_role(request):
    if not validate_cookie(request.COOKIES):
        return HttpResponseRedirect('/login/')
    try:
        player = get_cookie_content(request.COOKIES['credentials'])['p']
        player_game = gm.get_game_player_in(player)
        if (player_game.is_started):
            return HttpResponse(gm.get_player_role(player_game.code, player))
        return HttpResponse('BAD')
    except:
        return HttpResponse('BAD')

def statistics(request):
    response = HttpResponse(get_template('statistics.html').render({'slices' : Player.objects.all()}), request)
    return response