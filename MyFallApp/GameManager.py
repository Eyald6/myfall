# -*- coding: utf-8 -*-
from logger import logger
import threading
import time
import random
from models import *


class GameCodeExistsException(Exception):
	def __init__(self, *args, **kwargs):
		super(GameCodeExistsException, self).__init__(*args, **kwargs)


class GameCodeDoesNotExist(Exception):
	def __init__(self, *args, **kwargs):
		super(GameCodeDoesNotExist, self).__init__(*args, **kwargs)


class RoomPlayer(object):
	def __init__(self, name, display_name):
		self._name = name
		self._display_name = display_name


class Game(object):
	PLAYER_TIMEOUT = 60 * 60
	def __init__(self, code, admin_player, admin_display_name):
		self._admin = admin_player
		self._code = code
		self._lock = threading.RLock()
		self._players = {admin_player : time.time()}
		self._players_names = [ {'player' : admin_player, 'display_name' : admin_display_name} ]
		self._spies = None
		self._number_of_spies = None
		self._place = None
		self._places = []
		self._started = False
		self._first_player = None

	@property
	def places(self):
		return self._places
	
	@property
	def number_of_spies(self):
		return self._number_of_spies

	@property	
	def first_player(self):
		if not self._started:
			return None
		elif self._first_player is not None:
			return self._first_player
		self._first_player = random.choice(self.players.keys()) 
		return self._first_player

	@property
	def admin(self):
		return self._admin

	@property
	def code(self):
		return self._code
	
	@property
	def is_started(self):
		return self._started
	
	@property
	def players(self):
		return self._players

	@property
	def players_names(self):
		return self._players_names[::-1]
	
	@property
	def num_of_players(self):
		return len(self._players)

	def update_game(self):
		with self._lock:
			players = dict(self.players)
		for player, last_seen in players.iteritems():
			if time.time() - last_seen >= Game.PLAYER_TIMEOUT:
				with self._lock:
					del self._players[player]
					player_to_remove = [p for p in self._players_names if p['player'] == player][0]
					self._players_names.remove(player_to_remove)
					logger.info('%s Timedout!' % player)
					if self.admin == player and self.players:
						self._admin = random.choice(self.players.keys())
		# Return True if game is still alive (some one is in)
		return len(self.players)
	
	def _is_spy(self, player):
		return self._spies is not None and player in self._spies

	def join_game(self, player, display_name):
		with self._lock:
			self._players[player] = time.time()
			self._players_names.append({'player' : player, 'display_name' : display_name})
		return True

	def keepalive(self, player):
		with self._lock:
			if player not in self._players:
				return False
			self._players[player] = time.time()

	def remove_player(self, player):
		logger.info("SIGNALED REMOVE PLAYER %s" % player)
		with self._lock:
			player_names = self._players_names[:]
			if player in self.players:
				del self._players[player]
		player_to_remove = [p for p in player_names if p['player'] == player]
		with self._lock:
			if player_to_remove:
				self._players_names.remove(player_to_remove[0])
		if self.admin == player and self.players:
			self._admin = random.choice(self.players.keys())

	def start(self, set_name, number_of_spies):
		if self._started:
			return False
		with self._lock:
			print self._players.keys()
			players = self._players.keys()[:]
			self._spies = []
			for i in xrange(number_of_spies):
				selected = random.choice(players)
				Player.get_player(selected).chosen_as_spy()
				self._spies.append(selected)
				players.remove(selected)
			self._places = PlacesSet.get_set_places(set_name)
			self._place = random.choice(self.places)
			Place.get_place(self._place).chosen_as_place()
			self._started = True
		return True

	def stop(self):
		if not self._started:
			return False
		self._spies = None
		self._number_of_spies = None
		self._place = None
		self._places = []
		self._started = False
		self._first_player = None
		return True

	def get_player_role(self, player):
		if self._is_spy(player):
			return 'SPY'
		return u'את/ה נמצא/ת ב%s' % self._place


class GameManager(object):
	SAFE_LOBBY = True
	def __init__(self):
		self._games = {}
		self._managerThread = None
		self._lock = threading.RLock()

	@property
	def games(self):
		return self._games

	def _run(self):
		while True:
			with self._lock:
				games = dict(self.games)
			for code, game in games.iteritems():
				if not game.update_game():
					logger.info('Closing %s, no players left' % code)
					self.close_game(code)
			time.sleep(1)

	def start(self):
		with self._lock:
			logger.info('Starting GameManager')
			self._managerThread = threading.Thread(target=self._run)
			self._managerThread.start()

	def open_game(self, game_code, player, display_name):
		assert game_code
		if self.get_game_player_in(player) is not None:
			print 'Player Already in game'
			return False
		with self._lock:
			if game_code in self.games:
				raise GameCodeExistsException('A Game with the code "%s" already exists' % game_code)
			self._games[game_code] = Game(game_code, player, display_name)
			return True

	def close_game(self, game_code):
		with self._lock:
			del self._games[game_code]

	def start_game(self, game_code, player, set_name, number_of_spies):
		with self._lock:
			if game_code not in self.games:
				raise GameCodeDoesNotExist('A Game with the code "%s" does not exist' % game_code)
			if GameManager.SAFE_LOBBY and player != self.games[game_code].admin:
				return False
			self.games[game_code].start(set_name, number_of_spies)
			return True

	def stop_game(self, game_code, player):
		with self._lock:
			if game_code not in self.games:
				raise GameCodeDoesNotExist('A Game with the code "%s" does not exist' % game_code)
			if GameManager.SAFE_LOBBY and player != self.games[game_code].admin:
				return False
			self.games[game_code].stop()
			return True

	def join_game(self, game_code, player, display_name=None):
		if display_name is None:
			display_name = player
		if self.get_game_player_in(player) is not None:
			print 'Player Already in game'
			return False
		with self._lock:
			if game_code not in self.games:	
				raise GameCodeDoesNotExist('A Game with the code "%s" does not exist' % game_code)
			return self.games[game_code].join_game(player, display_name)

	def keepalive(self, player):
		if display_name is None:
			display_name = player
		player_game = self.get_game_player_in(player)
		if player_game is not None:
			player_game.keepalive(player, display_name)

	def remove_player(self, player):
		with self._lock:
			for game in self.games.values():
				game.remove_player(player)

	def get_player_role(self, game_code, player):
		with self._lock:
			if game_code not in self.games:
				raise GameCodeDoesNotExist('A Game with the code "%s" does not exist' % game_code)
			return self.games[game_code].get_player_role(player)

	def get_game_player_in(self, player):
		games = self.games.values()
		for game in games:
			if player in game.players:
				return game
		return None
