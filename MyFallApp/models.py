from __future__ import unicode_literals
from django.core.exceptions import ValidationError
from django.db import models
import json

# Create your models here.
class Player(models.Model):
    player_name = models.CharField(max_length=120, blank=False)
    times_a_spy = models.IntegerField(blank=False, default=0)

    def clean(self):
        if not self.player_name:
            raise ValidationError('No name was provided')
        assert Player.get_player(self.player_name, True) == None, ValidationError('Object Exists')

    def chosen_as_spy(self):
        self.times_a_spy += 1
        self.save()

    def clear_statistics(self):
        self.times_a_spy = 0
        self.save()

    def remove(self):
        for s in PlayersGroup.objects.all():
            s.remove_player_from_group(self.player_name)
        self.delete()

    @staticmethod
    def get_player(player_name, supress_not_found=False):
        for p in Player.objects.all():
            if p.player_name == player_name:
                return p
        if supress_not_found:
            return None
        raise ValueError('Player does not exist')

class PlacesSet(models.Model):
    set_name =  models.CharField(max_length=120, blank=False)
    places_json_list = models.CharField(max_length=0xffffffff, blank=False)
    times_played = models.IntegerField(blank=False, default=0)
    def clean(self):
        if not self.set_name:
            raise ValidationError('No name was provided')
        assert PlacesSet.get_set(self.set_name) == None, ValidationError('Object Exists')

    def add_place_to_set(self, place_name):
        assert Place.get_place(place_name) is not None
        if self.places_json_list:
            places_list = json.loads(self.places_json_list)
        else:
            places_list = []
        assert place_name not in places_list
        places_list.append(place_name)
        self.places_json_list = json.dumps(places_list)
        self.save()

    def remove_place_from_set(self, place_name):
        assert Place.get_place(place_name) is not None
        if not self.places_json_list:
            return
        places_list = json.loads(self.places_json_list)
        if not place_name in places_list:
            return
        places_list.remove(place_name)
        self.places_json_list = json.dumps(places_list)
        self.save()        

    def get_places(self):
        if not self.places_json_list:
            return
        return json.loads(self.places_json_list)

    def chosen_as_set(self):
        self.times_played += 1
        self.save()

    def clear_statistics(self):
        self.times_played = 0
        self.save()

    @staticmethod
    def get_set_places(set_name):
        for s in PlacesSet.objects.all():
            if s.set_name == set_name:
                return s.get_places()
        return None

    @staticmethod
    def get_set(set_name):
        for s in PlacesSet.objects.all():
            if s.set_name == set_name:
                return s
        return None        


class Place(models.Model):
    place_name = models.CharField(max_length=120, blank=False)
    times_played = models.IntegerField(blank=False, default=0)
    
    def clean(self):
        if not self.place_name:
            raise ValidationError('No name was provided')
        assert Place.get_place(self.place_name) == None, ValidationError('Object Exists')
    
    def chosen_as_place(self):
        self.times_played += 1
        self.save()

    def clear_statistics(self):
        self.times_played = 0
        self.save()

    def remove(self):
        for s in PlacesSet.objects.all():
            s.remove_place_from_set(self.place_name)
        self.delete()

    @staticmethod
    def get_place(place_name):
        for p in Place.objects.all():
            if p.place_name == place_name:
                return p
        return None


class PlayersGroup(models.Model):
    group_name =  models.CharField(max_length=120, blank=False)
    players_json_list = models.CharField(max_length=0xffffffff, blank=False)

    def clean(self):
        if not self.group_name:
            raise ValidationError('No name was provided')
        assert PlayersGroup.get_group(self.group_name) == None, ValidationError('Object Exists')

    def add_player_to_group(self, player_name):
        assert Player.get_player(player_name) is not None
        if self.players_json_list:
            players_list = json.loads(self.players_json_list)
        else:
            players_list = []
        assert player_name not in players_list
        players_list.append(player_name)
        self.players_json_list = json.dumps(players_list)
        self.save()

    def remove_player_from_group(self, player_name):
        assert Player.get_player(player_name) is not None
        if not self.players_json_list:
            return
        players_list = json.loads(self.players_json_list)
        if player_name not in players_list:
            return
        players_list.remove(player_name)
        self.players_json_list = json.dumps(players_list)
        self.save()        

    def get_players(self):
        if not self.players_json_list:
            return
        return json.loads(self.players_json_list)

    @staticmethod
    def get_group_players(group_name):
        for s in PlayersGroup.objects.all():
            if s.group_name == group_name:
                return s.get_players()
        return None

    @staticmethod
    def get_group(group_name):
        for s in PlayersGroup.objects.all():
            if s.group_name == group_name:
                return s
        return None
