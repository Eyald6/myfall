# -*- coding: utf-8 -*-
"""MyFall URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from . import views

urlpatterns = [
    #url(r'^admin/', admin.site.urls),
    url(r'^$', views.games),
    url(r'^login/', views.login),
    url(r'^connect/', views.connect),
    url(r'^games/', views.games),
    url(r'^statistics/', views.statistics),
    url(r'^api/games/list', views.games_list),
    url(r'^api/players', views.get_players),
    url(r'^api/sets', views.get_sets),
    url(r'^api/games/join', views.join_game),
    url(r'^api/games/quit', views.quit_game),
    url(r'^api/games/create', views.create_game),
    url(r'^api/games/start', views.start_game),
    url(r'^api/games/stop', views.stop_game),
    url(r'^api/games/role', views.get_current_role),
    url(r'^api/games/places', views.get_current_places),
    url(r'^manage/', views.manage),
    url(r'^api/manage/list', views.manage_list),
    url(r'^api/manage/remove', views.remove_obj),
    url(r'^api/manage/add', views.add_obj),
    url(r'^api/manage/link', views.link_obj),
    url(r'^api/manage/unlink', views.unlink_obj),
    url(r'.*', views.login),
]

urlpatterns += staticfiles_urlpatterns()