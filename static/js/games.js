global_games_list = null
set_chosen = null

function get_sets() {
  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
          if (xhttp.responseText != 'BAD') {
            list = JSON.parse(xhttp.responseText)
            for (var i = list.length - 1; i >= 0; i--) {
            	var set_name = list[i]
            	var li_child = document.createElement('a')
            	li_child.className = 'list-group-item'
            	li_child.innerHTML = set_name
            	li_child.setAttribute('onclick', 'choose_set("' + set_name + '")')
            	set_modal_list.appendChild(li_child)
            }
          } else {
          	alert('BAD')
          }
        }
    }
    xhttp.open('GET', '/api/sets', true)
    xhttp.send()
}	

function mark_cell(cell) {
	if ('false' == cell.getAttribute('marked')) {
		content = cell.innerHTML
		cell.innerHTML = '<del>' + content + '</del>'
		cell.setAttribute('marked', 'true')
	} else {
		cell.setAttribute('marked', 'false')
		content = cell.firstChild.innerHTML
		cell.innerHTML = content
	}
}

function match_value(old_list, new_list, button) {
	new_list.value = old_list.value
	for (var i = new_list.childNodes.length - 1; i >= 0; i--) {
		var name_element = null
		if (new_list.childNodes[i].getAttribute('marked') == 'true') {
			name_element = new_list.childNodes[i].childNodes[0].childNodes[1]
		} else {
			name_element = new_list.childNodes[i].childNodes[1]
		}
		if (new_list.value != 0) {
			name_element.style.display = ''
		} else {
			name_element.style.display = 'none'
		}
	}
	if (new_list.value != 0) {
		button.innerHTML = 'הסתר שמות'
		button.className = 'btn btn-danger'
	} else {
		button.innerHTML = 'הצג שמות'
		button.className = 'btn btn-info'
	}
}

function switch_names(new_list, button) {
	new_list.value = !new_list.value
	for (var i = new_list.childNodes.length - 1; i >= 0; i--) {
		var name_element = null
		if (new_list.childNodes[i].getAttribute('marked') == 'true') {
			name_element = new_list.childNodes[i].childNodes[0].childNodes[1]
		} else {
				name_element = new_list.childNodes[i].childNodes[1]
		}
		if (new_list.value != 0) {
			name_element.style.display = ''
		} else {
			name_element.style.display = 'none'
		}
	}
	if (new_list.value != 0) {
		button.innerHTML = 'הסתר שמות'
		button.className = 'btn btn-danger'
	} else {
		button.innerHTML = 'הצג שמות'
		button.className = 'btn btn-info'
	}
}

function generate_game_html(game, inGame, old_div) {
	var div_panel = document.createElement('div');
	div_panel.style.width = '100%'
	div_panel.style.width = 'padding-top: 2%'
	div_panel.style.width = 'padding-bottom: 2%'
	div_panel.className =  'panel panel-default'
	var div_heading = document.createElement('div')
	div_heading.className = 'panel-heading'
	if (game.is_started) {
		div_heading.style.backgroundColor = 'lightgreen'
		var started_header = document.createElement('span')
		started_header.style.color = 'green'
		started_header.innerHTML = '<b>Started - </b>'
		div_heading.appendChild(started_header)
	}
	div_heading.innerHTML += '<i><b>' + game.code + '</b></i><br/> (' + game.num_of_players + ' Players)'

	var players_list = document.createElement('div')
	players_list.id = game.code
	players_list.value = old_div.value
	players_list.className = 'list-group row'
	players_list.style.width = "100%"
	for (var i = game.players_names.length - 1; i >= 0; i--) {
		var player_entry = document.createElement('a')
		if (inGame == 'True') {
			player_entry.className = 'btn '
		}
		old_player = get_player(game.players_names[i]['player'], old_div)
		player_entry.className += 'list-group-item col-md-6'
		player_entry.style.border = "ridged"
		player_entry.style.borderWidth = "1px"
		player_entry.innerHTML = game.players_names[i]['display_name']
		player_entry.setAttribute('player_name', game.players_names[i]['player'])
		var real_name = document.createElement('span')
		real_name.style.color = 'orange'
		real_name.innerHTML = ' - ' + game.players_names[i]['player']
		if (inGame == 'True') {
			player_entry.setAttribute('onclick', 'mark_cell(this)')
		}
		player_entry.appendChild(real_name)
		if (game.players_names[i]['player'] == game.admin) {
			player_entry.className += ' active'
			var admin_indicator = document.createElement('span')
			admin_indicator.style.color = 'lightgreen'
			admin_indicator.innerHTML = ' - Admin'
			player_entry.appendChild(admin_indicator)
		}
		if (game.players_names[i]['player'] == game.first_player) {
			var first_indicator = document.createElement('span')
			first_indicator.style.color = 'red'
			first_indicator.innerHTML = ' - 1st'
			player_entry.appendChild(first_indicator)	
		}
		player_entry.setAttribute('marked', 'false')
		if (inGame == 'True' && old_player.getAttribute('marked') == 'true') {
			mark_cell(player_entry)
		}
		players_list.appendChild(player_entry)
	}
	var toggle_names = document.createElement('button')
	toggle_names.type = 'button'
	try {
		players_list.value = !old_div.childNodes[0].getAttribute('value')
		match_value(old_div, players_list, toggle_names)
	} catch(err) {
		match_value(players_list, players_list, toggle_names)
	}
	toggle_names.setAttribute('onclick', 'switch_names(this.parentNode.childNodes[1], this)')
	div_panel.appendChild(div_heading)
	div_panel.appendChild(players_list)
	div_panel.appendChild(toggle_names)
	return div_panel
}

function create_admin_panel(isStarted) {
  var admin_panel = document.createElement('div')
  admin_panel.className = 'panel-heading'
  var start_button = document.createElement('button')
  start_button.type = 'button'
  if (!isStarted) {
	  start_button.className = 'btn btn-success'
	  start_button.innerHTML = 'Start Game'
	  start_button.setAttribute('data-toggle', 'modal')
	  start_button.setAttribute('data-target', '#' + 'sets_modal')
  } else{
	  start_button.className = 'btn btn-danger'
	  start_button.innerHTML = 'End Game'
	  start_button.setAttribute('onclick', 'stop_game()')
  }
  admin_panel.appendChild(start_button)
  return admin_panel
}

function get_game_value(games, code) {
	for (var i = games.length - 1; i >= 0; i--) {
		if (games[i].code == code) {
			return games[i].value
		}
	}
	return 0
}

function get_empty_game() {
	var new_game_div = document.createElement('div')
	var new_game = document.createElement('div')
	new_game.setAttribute('value', 0)
	new_game_div.appendChild(new_game)
	return new_game_div
}

function get_player(player_name, old_div) {
	for (var i = old_div.childNodes.length - 1; i >= 0; i--) {
		if (player_name == old_div.childNodes[i].getAttribute('player_name')) {
			return old_div.childNodes[i]
		}
	}
	var new_player = document.createElement('div')
	new_player.setAttribute('marked', 'false')
	return new_player
}

function get_old_place(place_name, old_div) {
	for (var i = old_div.childNodes.length - 1; i >= 0; i--) {
		if (old_div.childNodes[i].getAttribute('marked') == 'false') {
			if (place_name == old_div.childNodes[i].innerHTML) {
				return old_div.childNodes[i]
			}
		} else {
			if (place_name == old_div.childNodes[i].firstChild.innerHTML) {
				return old_div.childNodes[i]
			}
		}
	}
	var new_player = document.createElement('div')
	new_player.setAttribute('marked', 'false')
	return new_player
}

function parse_games_list(json_str, inGame) {
	var games = JSON.parse(json_str)
	if (games.length <= 0) {
		role.style.display = 'none'
		places_div.style.display = 'none'
	}
	number_of_rooms.innerHTML = games.length
	rooms_number.innerHTML = games.length
	if (inGame == 'True') {
		actions_div.style.display = 'none'
	} else { 
		actions_div.style.display = ''
	}
	for (var i = games.length - 1; i >= 0; i--) {
		game = JSON.parse(games[i])
		old_div = document.getElementById(game.code)
		if (old_div == null) {
			old_div = get_empty_game()
			div_to_delete = null
		} else {
			div_to_delete = document.getElementById(game.code).parentNode
		}
		var game_html = generate_game_html(game, inGame, old_div)
		var button = document.createElement('button')
		button.type = 'button'
		button.setAttribute('style', 'margin: 2%')
		if (inGame == 'True') {
			button.className = 'btn btn-warning'
			button.setAttribute('onclick', 'quit_game()')
			button.innerHTML = 'צא'
		} else {
			button.className = 'btn btn-success'
			button.setAttribute('onclick', 'join_game("' + game.code + '")')
			button.innerHTML = 'הצטרף'
		}
		game_html.appendChild(button)
		if (inGame == 'True') {
			game_html.appendChild(create_admin_panel(game.is_started))
			if (game.is_started) {
				get_current_role()
				get_current_places()
			} else {
				role.style.display = 'none'
				places_div.style.display = 'none'
			}
		} else {
			role.style.display = 'none'
			places_div.style.display = 'none'
		}
		try {
			games_list.removeChild(div_to_delete)
		} catch(err) {

		}
		games_list.appendChild(game_html)
		games_list.style.width = "100%"
	}
	for (var i = games_list.childNodes.length - 1; i >= 0; i--) {
		game_code = games_list.childNodes[i].childNodes[1].id
		var found = false
		for (var j = games.length - 1; j >= 0; j--) {
			if (game_code == JSON.parse(games[j]).code) {
				found = true
				break
			}
		}
		if (!found) {
			games_list.removeChild(games_list.childNodes[i])
		}
	}
}

function update_games() {
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
			var inGame = xhttp.getResponseHeader('inGame')
  			parse_games_list(xhttp.responseText, inGame)
  			if (inGame == 'True') {
  				actions_tab_a.style.display = 'none'
  				rooms_tab_a.click()
  			} else {
  				actions_tab_a.style.display = ''
  			}
      	}
  	}
  	xhttp.open('GET', '/api/games/list', true)
  	xhttp.send()
}

function join_game(game_code) {
	update_games()
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
  			if (xhttp.responseText != 'BAD') {
  				update_games()
  			} else {
  				alert('Could not join game')
  			}
      	}
  	}
  	xhttp.open('GET', '/api/games/join?gc=' + game_code, true)
  	xhttp.send()
}

function quit_game() {
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
  			update_games()
  			actions_tab_a.style.display = ''
  		}
  	}
  	xhttp.open('GET', '/api/games/quit/', true)
  	xhttp.send()
}

function create_game() {
	game_code = game_code_input.value
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText == 'OK') {
	            update_games()
	            actions_tab_a.style.display = 'none'
	            rooms_tab_a.click()
      		} else {
      			alert('Could not create game')
      		}
      	}
  	}
  	xhttp.open('POST', '/api/games/create/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send('gc=' + game_code)
}

function choose_set(set_name) {
	for (var i = set_modal_list.childNodes.length - 1; i >= 0; i--) {
		item = set_modal_list.childNodes[i]
		if (item.innerHTML != set_name) {
			item.className = 'list-group-item'
		} else {
			item.className = 'list-group-item active'
		}
	}
	set_chosen = set_name
}

function start_game() {
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText == 'OK') {
	            update_games()
      		} else {
      			alert('Could not start game')
      		}
      	}
  	}
  	xhttp.open('POST', '/api/games/start/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send('set_name=' + set_chosen + '&num_spies=' + number_of_spies.innerHTML)
}


function stop_game() {
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText == 'OK') {
	            update_games()
      		} else {
      			alert('Could not stop game')
      		}
      	}
  	}
  	xhttp.open('POST', '/api/games/stop/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send()
}

function spy_up() {
	var new_value = parseInt(number_of_spies.innerHTML) + 1
	number_of_spies.innerHTML = new_value.toString()
}

function spy_down() {
	var new_value = parseInt(number_of_spies.innerHTML) - 1
	number_of_spies.innerHTML = new_value.toString()
}

function get_current_role() {
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText != 'BAD') {
	            role_response = xhttp.responseText
	            if (role_response == 'SPY') {
	            	role_header.innerHTML = 'את/ה המרגל/ת'
	            } else {
	            	role_header.innerHTML = role_response
	            }
				role.style.display = ''
      		}
      	}
  	}
  	xhttp.open('GET', '/api/games/role/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send()
}

function get_current_places() {
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText != 'BAD') {
 				old_div = game_places_list
	            places = JSON.parse(xhttp.responseText)
	            generate_places_list(places)
				places_div.style.display = ''
      		}
      	}
  	}
  	xhttp.open('GET', '/api/games/places/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send()
}

function generate_places_list(places) {
	old_div = game_places_list
	for (var i = places.length - 1; i >= 0; i--) {
		var new_place = places[i]
		var old_place = get_old_place(new_place, old_div)
		var place_entry = document.createElement('a')
		place_entry.className = 'list-group-item col-xs-6'
		place_entry.style.border = "ridged"
		place_entry.style.borderWidth = "1px"
		place_entry.setAttribute('onclick', 'mark_cell(this)')
		place_entry.innerHTML = new_place
		place_entry.setAttribute('href', '#.')
		place_entry.setAttribute('marked', 'false')
		if (old_place.getAttribute('marked') == 'true') {
			mark_cell(place_entry)
		}
		try {
			game_places_list.appendChild(place_entry)
			game_places_list.removeChild(old_place)
		} catch(err) {
		}
			
	}
}