global_players_list = null

function choose_player(player_name) {
	if (player_name != "") {
		player.innerHTML = player_name
		bc = document.getElementById('display_div')
		bc.style.display = ""
	}
}

function connect() {
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText == 'OK') {
	            window.location.assign("/games/")
      		} else {
      			alert(xhttp.responseText)
      		}
      	}
  	}
  	xhttp.open('POST', '/connect/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send('dn=' + display_name.value + '&p=' + player.innerHTML)
}

function get_players() {
  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4 && xhttp.status == 200) {
          if (xhttp.responseText != 'BAD') {
            global_players_list = JSON.parse(xhttp.responseText)
            update_players(global_players_list)
          }
        }
    }
    xhttp.open('GET', '/api/players', true)
    xhttp.send()
}

function update_players(list) {
  number_of_players.innerHTML = list.length
  players_list.innerHTML = ''
  for (var i = list.length - 1; i >= 0; i--) {
    player_name = list[i]
    var li_child = document.createElement('li')
    li_child.setAttribute('onclick', 'choose_player("' + player_name + '")')
    var li_a = document.createElement('a')
    li_a.innerHTML = player_name
    li_child.appendChild(li_a)
    players_list.appendChild(li_child)
  }
}

function on_text_change() {
  list = []
  for (var i = global_players_list.length - 1; i >= 0; i--) {
    player_name = global_players_list[i]
    if (player_name.startsWith(player_search.value)) {
      list.push(player_name)
    }
  }
  update_players(list)
}