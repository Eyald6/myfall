global_groups_list = null
global_players = null
global_places = null
global_sets_list = null

function get_buffering_object() {
	var buffering = document.createElement('img')
	buffering.setAttribute('src', '/static/img/buffering.gif')
	return buffering
}

function update_values() {
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
  			parse_values(xhttp.responseText)
      	}
  	}
  	xhttp.open('GET', '/api/manage/list', true)
  	xhttp.send()
}

function parse_values(response) {
	values = JSON.parse(response)
	players =  values['players']
	groups = values['groups']
	groups_list = values['groups_list']
	places = values['places']
	sets = values['sets']
	sets_list = values['sets_list']
	global_groups_list = groups_list
	global_sets_list = sets_list
	global_players = players
	global_places = places

	build_list('players', players)
	build_list('groups', groups)
	build_list('places', places)
	build_list('sets', sets)
}

function build_group_modal(name) {
	group_name.innerHTML = name
	list_element = groups_modal_list
	existing_element = groups_modal_existing
	existing_element.innerHTML = ''
	list_element.innerHTML = ''
	list_obj = global_players
	if (list_obj == null) {
		return
	}
	for (var i = list_obj.length - 1; i >= 0; i--) {
		child = list_obj[i]
		var li_child = document.createElement('li')
		li_child.className = 'li_row'
		var existing_objects = JSON.parse(global_groups_list[name])
		if (existing_objects == null) {
			existing_objects = []
		}
		if (!obj_in_list(child, existing_objects)) {
			li_child.setAttribute('onclick', 'add_player("' + child + '")')
			li_child.innerHTML += child + ' '
			list_element.appendChild(li_child)
		}
	}
	var existing_objects = JSON.parse(global_groups_list[name])
	if (existing_objects == null) {
		existing_objects = []
	}
	for (var i = existing_objects.length - 1; i >= 0; i--) {
		existing_element = groups_modal_existing
		child = existing_objects[i]
		var li_child = document.createElement('li')
		li_child.className = 'li_row'
		li_child.setAttribute('onclick', 'remove_player("' + child + '")')
		li_child.innerHTML += child
		existing_element.appendChild(li_child)
	}
}

function obj_in_list(obj, list) {
	for (var j = list.length - 1; j >= 0; j--) {
		if (list[j] == obj) {
			return true
		}
	}
	return false
}

function build_set_modal(name) {
	set_name.innerHTML = name
	list_element = sets_modal_list
	existing_element = sets_modal_existing
	existing_element.innerHTML = ''
	list_element.innerHTML = ''
	list_obj = global_places
	if (list_obj == null) {
		return
	}
	for (var i = list_obj.length - 1; i >= 0; i--) {
		child = list_obj[i]
		var li_child = document.createElement('li')
		li_child.className = 'li_row'
		var existing_objects = JSON.parse(global_sets_list[name])
		if (existing_objects == null) {
			existing_objects = []
		}
		if (!obj_in_list(child, existing_objects)) {
			li_child.setAttribute('onclick', 'add_place("' + child + '")')
			li_child.innerHTML += child + ' '
			list_element.appendChild(li_child)
		}
	}
	var existing_objects = JSON.parse(global_sets_list[name])
	if (existing_objects == null) {
		existing_objects = []
	}
	for (var i = existing_objects.length - 1; i >= 0; i--) {
		existing_element = sets_modal_existing
		child = existing_objects[i]
		var li_child = document.createElement('li')
		li_child.className = 'li_row'
		li_child.setAttribute('onclick', 'remove_place("' + child + '")')
		li_child.innerHTML += child
		existing_element.appendChild(li_child)
	}
}	

function choose_place(place_name) {
	set_chosen.value = place_name
	set_chosen.innerHTML = player_name
}

function choose_player(player_name) {
	group_chosen.value = player_name
	group_chosen.innerHTML = player_name
}

function add_player(obj_name) {
	list_name = group_name.innerHTML
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText == 'OK') {
	            success_bootstrap(obj_name + " Was added successfuly to " + list_name)
	            update_values()
	            l = JSON.parse(global_groups_list[list_name])
	            l.push(obj_name)
	            global_groups_list[list_name] = JSON.stringify(l)
	            build_group_modal(list_name)
      		} else {
      			failure_bootstrap('Could not add object ' + obj_name + ' to ' + list_name)
      		}
      	}
  	}
	xhttp.open('POST', '/api/manage/link/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send('obj_name=' + obj_name + '&list_name=' + list_name + '&link_to=groups')
}

function add_place(obj_name) {
	list_name = set_name.innerHTML
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText == 'OK') {
	            success_bootstrap(obj_name + " Was added successfuly to " + list_name)
	            update_values()
	            l = JSON.parse(global_sets_list[list_name])
	            l.push(obj_name)
	            global_sets_list[list_name] = JSON.stringify(l)
	            build_set_modal(list_name)
      		} else {
      			failure_bootstrap('Could not add object ' + obj_name + ' to ' + list_name)
      		}
      	}
  	}
	xhttp.open('POST', '/api/manage/link/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send('obj_name=' + obj_name + '&list_name=' + list_name + '&link_to=sets')
}

function build_list(list_name, list) {
	list_element = document.getElementById(list_name + '_list')
	list_element.innerHTML = get_buffering_object()
	for (var i = list.length - 1; i >= 0; i--) {
		var obj_row = document.createElement('tr')
		var name_field = document.createElement('td')
		var delete_field = document.createElement('td')
		name_field.innerHTML = list[i]
		var button = document.createElement('button')
		button.className = 'btn btn-danger'
		button.type = 'button'
		var button_span = document.createElement('span')
		button_span.className = 'glyphicon glyphicon-remove'
		button.appendChild(button_span)
		delete_field.appendChild(button)
		delete_field.setAttribute('onclick', 'remove_obj("' + list_name + '", "' + list[i] + '")')

		obj_row.appendChild(name_field)
		
		if (list_name == 'sets') {
			var add_field = document.createElement('td')
			var add_button = document.createElement('button')
			add_button.className = 'btn btn-success'
			add_button.type = 'button'
			add_button.innerHTML = 'Manage'
			add_button.setAttribute('data-toggle', 'modal')
			add_button.setAttribute('data-target', '#' + 'sets_modal')
			add_field.appendChild(add_button)
			add_field.setAttribute('onclick', 'build_set_modal("' + list[i] +'")')
			obj_row.appendChild(add_field)
		} else if (list_name == 'groups') {
			var add_field = document.createElement('td')
			var add_button = document.createElement('button')
			add_button.innerHTML = 'Manage'
			add_button.setAttribute('data-toggle', 'modal')
			add_button.setAttribute('data-target', '#' + 'groups_modal')
			add_button.className = 'btn btn-success'
			add_button.type = 'button'
			add_field.appendChild(add_button)
			add_field.setAttribute('onclick', 'build_group_modal("' + list[i] +'")')
			obj_row.appendChild(add_field)
		}

		obj_row.appendChild(delete_field)
		list_element.appendChild(obj_row)
	}
	list_element.removeChild(list_element.firstChild)
}

function remove_obj(list_name, obj_name) {
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText == 'OK') {
	            success_bootstrap(obj_name + " Was deleted successfuly from " + list_name)
	            update_values()
      		} else {
      			failure_bootstrap('Could not delete object ' + obj_name + ' from ' + list_name)
      		}
      	}
  	}
	xhttp.open('POST', '/api/manage/remove/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send('obj_name=' + obj_name + '&list_name=' + list_name)
}

function choose_list(list_name) {
	while (controller.firstChild) {
		controller.removeChild(controller.firstChild)
	}
	if (list_name == 'players' || list_name == 'places') {
		var input_name = document.createElement('input')
		input_name.setAttribute('type', 'text')
		input_name.className = 'form-control'
		input_name.id = 'input_field'
		add_button.setAttribute('onclick', 'add_obj("' + list_name + '", this.value)')
		controller.appendChild(input_name)
	} else if (list_name == 'sets' || list_name == 'groups') {
		var list_obj = null
		if (list_name == 'sets') {
			list_obj = document.getElementById('sets_list')

		} else {
			list_obj = document.getElementById('groups_list')
		}
		var form_div = document.createElement('div')
		form_div.className = 'form-group'
		var toggle_button = document.createElement('button')
		toggle_button.className = 'btn btn-primary dropdown-toggle'
		toggle_button.type = 'button'
		toggle_button.setAttribute('data-toggle', 'dropdown')
		toggle_button.innerHTML = '<span class="caret"></span>בחר'
		var drop_down = document.createElement('ul')
		drop_down.className = 'dropdown-menu'
		drop_down.id = 'input_field'
		for (var i = list_obj.length - 1; i >= 0; i--) {
			var value = document.createElement('li')
			value.innerHTML = list_obj[i]
			drop_down.appendChild(value)
		}
		add_button.setAttribute('onclick', 'add_obj("' + list_name + '")')
		form_div.appendChild(toggle_button)
		form_div.appendChild(drop_down)
		controller.appendChild(form_div)
	}
}

function add_obj(list_name) {
	obj_name = document.getElementById(list_name + '_input').value
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText == 'OK') {
	            success_bootstrap(obj_name + " Was added successfuly to " + list_name)
	            document.getElementById(list_name + '_input').value = ''
	            update_values()
      		} else {
      			failure_bootstrap('Could not add object ' + obj_name + ' to ' + list_name)
      		}
      	}
  	}
	xhttp.open('POST', '/api/manage/add/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send('obj_name=' + obj_name + '&list_name=' + list_name)
}

function success_bootstrap(message) {
	var alert_div = document.createElement('div')
  alert_div.className = 'alert alert-success'
  var close_button = document.createElement('a')
  close_button.className = 'close'
  close_button.setAttribute('data-dismiss', 'alert')
  close_button.setAttribute('aira-label', 'close')
  close_button.setAttribute('href', '#')
  close_button.innerHTML = '&times;'
  alert_div.appendChild(close_button)
  var message_span = document.createElement('span')
  message_span.innerHTML = message
  alert_div.appendChild(message_span)
  alerts.appendChild(alert_div)
  setTimeout(function() {alerts.removeChild(alert_div); }, 3000);
}

function failure_bootstrap(message) {
  var alert_div = document.createElement('div')
  alert_div.className = 'alert alert-danger'
  var close_button = document.createElement('a')
  close_button.className = 'close'
  close_button.setAttribute('data-dismiss', 'alert')
  close_button.setAttribute('aira-label', 'close')
  close_button.setAttribute('href', '#')
  close_button.innerHTML = '&times;'
  alert_div.appendChild(close_button)
  var message_span = document.createElement('span')
  message_span.innerHTML = message
  alert_div.appendChild(message_span)
  alerts.appendChild(alert_div)
  setTimeout(function() {alerts.removeChild(alert_div); }, 3000);
}

function remove_player(obj_name) {
	list_name = group_name.innerHTML
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText == 'OK') {
	            success_bootstrap(obj_name + " Was removed successfuly from " + list_name)
	            update_values()
	            l = JSON.parse(global_groups_list[list_name])
	            var index = l.indexOf(obj_name)
	            if (index != -1) {
	            	l.splice(index, 1)
	            	global_groups_list[list_name] = JSON.stringify(l)
	            }
	            build_group_modal(list_name)
      		} else {
      			failure_bootstrap('Could not remove object ' + obj_name + ' from ' + list_name)
      		}
      	}
  	}
	xhttp.open('POST', '/api/manage/unlink/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send('obj_name=' + obj_name + '&list_name=' + list_name + '&link_to=groups')
}

function remove_place(obj_name) {
	list_name = set_name.innerHTML
	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
		if (xhttp.readyState == 4 && xhttp.status == 200) {
      		if (xhttp.responseText == 'OK') {
	            success_bootstrap(obj_name + " Was removed successfuly from " + list_name)
	            update_values()
	            l = JSON.parse(global_sets_list[list_name])
	            var index = l.indexOf(obj_name)
	            if (index != -1) {
	            	l.splice(index, 1)
	            	global_sets_list[list_name] = JSON.stringify(l)
	            }
	            build_set_modal(list_name)
      		} else {
      			failure_bootstrap('Could not remove object ' + obj_name + ' from ' + list_name)
      		}
      	}
  	}
	xhttp.open('POST', '/api/manage/unlink/', true)
  	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  	xhttp.send('obj_name=' + obj_name + '&list_name=' + list_name + '&link_to=sets')
}